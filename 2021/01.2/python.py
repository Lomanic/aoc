#!/usr/bin/env python3

t = []
with open('input', 'r') as f:
    t = [int(x) for x in f]

tt = []
for yo in [t[i-3:i] for i in range(3, len(t)+1)]:
    susum = 0
    for yoyo in yo:
        susum += yoyo
    tt.append(susum)

#print(tt)

sum = 0
for i in range(1, len(tt)):
    if tt[i-1] < tt[i]:
        sum += 1
print(sum)

