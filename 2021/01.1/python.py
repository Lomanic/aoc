#!/usr/bin/env python3

with open('input', 'r') as f:
    t = [int(x) for x in f]
    sum = 0
    for i in range(1, len(t)):
        if t[i-1] < t[i]:
            sum += 1
    print(sum)
