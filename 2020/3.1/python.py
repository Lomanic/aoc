#!/usr/bin/env python

import sys

def parse_input(file):
    f = open(file, 'r')
    the_list = []
    while True:
        line = f.readline()
        line = line.rstrip()
        if not line: break
        the_list.append(line)
    return the_list

def traverse(the_list, x_increment, y_increment):
    path = ""
    x = 0
    y = 0
    while x < len(the_list) - 1:
        x += x_increment
        y += y_increment
        path += the_list[x][y % len(the_list[x])]
    return len([c for c in path if c == "#"])

the_list = parse_input("input")
print traverse(the_list, 1, 3)
