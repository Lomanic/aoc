package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const preamble = 25

func main() {
	var list []int
	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	r := bufio.NewReader(f)
	i := 0
	for {
		line, err := r.ReadString('\n')
		if err != nil {
			break
		}

		currentInt, _ := strconv.Atoi(strings.TrimSuffix(line, "\n"))
		list = append(list, currentInt)

		if preamble <= i {
			prev25 := list[i-preamble : i]
			// fmt.Println(prev25)
			found := false
		Loop:
			for xpos, x := range prev25 {
				for ypos, y := range prev25[:len(prev25)-1] { // don't check last one in list as it will be already sumed by the previous loops
					if xpos == ypos { // don't sum the same number with itself
						continue
					}
					if x+y == currentInt {
						// fmt.Printf("%v + %v = %v\n", x, y, currentInt)
						found = true
						break Loop
					}
				}
			}
			if !found {
				fmt.Println(currentInt)
				os.Exit(0)
			}
		}

		i++
	}
}
