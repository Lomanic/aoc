package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strings"
)

type seat struct {
	row            int // 128 rows: rows 0 through 127
	column         int // 8 columns
	id             int
	representation string
}

func readLines(filename string) ([]string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return []string{""}, err
	}
	defer f.Close()

	var ret []string

	r := bufio.NewReader(f)
	for {
		line, err := r.ReadString('\n')
		if err != nil {
			break
		}
		ret = append(ret, strings.Trim(line, "\n"))
	}
	return ret, nil
}

func newSeat(input string) seat {
	minRow := 0
	maxRow := 127
	minCol := 0
	maxCol := 7
	s := seat{
		row:            0,
		column:         0,
		representation: input,
	}

	for _, c := range input[:7] { // row
		switch c {
		case 'F':
			maxRow = maxRow - ((maxRow - minRow) / 2) - 1
		case 'B':
			minRow = minRow + ((maxRow - minRow) / 2) + 1
		default:
			panic(fmt.Sprintf("wtf %v %v", input, string(c)))
		}
	}
	s.row = maxRow

	for _, c := range input[7:] { // col
		switch c {
		case 'L':
			maxCol = maxCol - ((maxCol - minCol) / 2) - 1
		case 'R':
			minCol = minCol + ((maxCol - minCol) / 2) + 1
		default:
			panic(fmt.Sprintf("wtf %v %v", input, string(c)))
		}
	}
	s.column = maxCol

	s.id = (s.row * 8) + s.column

	return s
}

func main() {
	lines, err := readLines("input")
	if err != nil {
		panic(err)
	}
	var seats []seat
	for _, line := range lines {
		seat := newSeat(line)
		seats = append(seats, seat)
	}
	sort.Slice(seats, func(i, j int) bool { return seats[i].id < seats[j].id })

	lowestID := seats[0].id
	highestID := seats[len(seats)-1].id

	fmt.Println(lowestID, highestID, len(seats), highestID-lowestID)

	for n, id := 0, lowestID; id < highestID; n, id = n+1, id+1 { // IDs are sequential, because of the formula to calculate them
		if seats[n].id != id {
			fmt.Println(id)
			os.Exit(0)
		}
	}
}
