#!/bin/bash

while read -r LINE; do
    LIST+=("$LINE")
done < "input"

RET=0
GROUP=()
for LINE in "${LIST[@]}"; do
    if [[ -z "$LINE" ]]; then
        LEN=${#GROUP[@]}
        A=$(printf '%s\n' "${GROUP[@]}" | grep -o . | sort | uniq -c | grep -c " $LEN ")
        RET=$(( RET + A ))
        GROUP=()
        continue
    fi
    GROUP+=("$LINE")
done

if [[ "${#GROUP[@]}" -ne 0 ]]; then # last empty line is swallowed...
    LEN=${#GROUP[@]}
    A=$(printf '%s\n' "${GROUP[@]}" | grep -o . | sort | uniq -c | grep -c " $LEN ")
    RET=$(( RET + A ))
fi

echo "${RET}"
