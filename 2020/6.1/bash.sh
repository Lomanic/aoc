#!/bin/bash

while read -r LINE; do
    LIST+=("$LINE")
done < "input"

RET=0
GROUP=""
for LINE in "${LIST[@]}"; do
    if [[ -z "$LINE" ]]; then
        A=$(grep -o . <<<"$GROUP" | sort -u | wc -l)
        RET=$(( RET + A ))
        # echo "$GROUP $A"
        GROUP=""
    fi
    # printf "$LINE\n"
    GROUP="${GROUP}${LINE}"
done

if [[ -n "$GROUP" ]]; then # last empty line is swallowed...
    A=$(grep -o . <<<"$GROUP" | sort -u | wc -l)
    RET=$(( RET + A ))
fi

echo "${RET}"
