#!/bin/bash

IFS=$'\n' read -d "" -ra LIST < "input"

VALID=0
for LINE in "${LIST[@]}"; do
	POLICY=${LINE%%:*}
    LETTER=${POLICY##* }
    OCCURENCES=${POLICY%% *}
    MIN=${OCCURENCES%%-*}
    MAX=${OCCURENCES##*-}
    PASSWORD=${LINE##*:}
    PASSWORD=${LINE##* }

    FOUND=0
    for (( I=0; I<${#PASSWORD}; I++ )); do
        if [[ "${PASSWORD:$I:1}" == "$LETTER" ]]; then
            FOUND=$(( FOUND + 1))
        fi
    done
    if [[ $FOUND -ge $MIN ]] && [[ $FOUND -le $MAX ]]; then
        VALID=$(( VALID + 1 ))
    fi
done

echo $VALID
