package main

import (
	"bufio"
	"fmt"
	"os"
	// "sort"
	"regexp"
	"strings"
)

type bag struct {
	parent *bag
	color  string
}

func readLines(filename string) ([]string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return []string{""}, err
	}
	defer f.Close()

	var ret []string

	r := bufio.NewReader(f)
	for {
		line, err := r.ReadString('\n')
		if err != nil {
			break
		}
		ret = append(ret, strings.Trim(line, "\n"))
	}
	return ret, nil
}

func newBag(color string) bag {
	b := bag{
		color: color,
	}

	return b
}

func bagFromColor(bags []bag, color string) *bag {
	for _, bag := range bags {
		if bag.color == color {
			return &bag
		}
	}
	return nil
}

func main() {
	lines, err := readLines("input")
	if err != nil {
		panic(err)
	}
	// var bags [string]bag{}
	var bags = make(map[string]bag)

	// light red bags contain 1 bright white bag, 2 muted yellow bags.
	re := regexp.MustCompile(`(\w+ \w+) bags`)

	for _, line := range lines {
		splittedLine := strings.Split(line, " contain ")
		if len(splittedLine) != 2 {
			continue
		}

		containerMatches := re.FindStringSubmatch(splittedLine[0])
		if containerMatches == nil || len(containerMatches) != 2 {
			continue
		}
		container := newBag(containerMatches[1])
		fmt.Printf("%+v\n", container)
		if _, ok := bags[container.color]; !ok {
			bags[container.color] = container
		}

		containedMatches := re.FindAllStringSubmatch(splittedLine[1], -1)
		if containedMatches == nil || len(containedMatches) < 1 {
			continue
		}
		for _, submatch := range containedMatches {
			if submatch[1] == "no other" {
				continue
			}
			var bagggggg = newBag(submatch[1])
			bagggggg.parent = &container
			bags[bagggggg.color] = bagggggg
			fmt.Printf("%+v\n", bagggggg)
		}
	}

	fmt.Printf("%+v\n", bags)
	for _, b := range bags {
		fmt.Println("bag", b.color, "is in", b.parent.color)
	}
}
