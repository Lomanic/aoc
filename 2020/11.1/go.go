package main

import (
	"fmt"
	"io/ioutil"
	"log"
	// "os"
	"strings"
)

type seat struct {
	x        int
	y        int
	occupied bool
}

func (s seat) adgacent(seats []seat) []seat {
	var _adgacent []seat
	for _, ss := range seats {
		if s.x == ss.x && s.y == ss.y {
			continue
		}
		if s.x-1 <= ss.x && ss.x <= s.x+1 &&
			s.y-1 <= ss.y && ss.y <= s.y+1 {
			_adgacent = append(_adgacent, ss)
		}
	}
	return _adgacent
}

func main() {
	var seats []seat
	content, err := ioutil.ReadFile("input")
	if err != nil {
		log.Fatal(err)
	}

	for y, line := range strings.Split(string(content), "\n") {
		if line == "" {
			continue
		}
		for x, character := range line {
			switch character {
			case 'L':
				s := seat{x: x, y: y}
				seats = append(seats, s)
			}
		}
	}
	// fmt.Println(seats)
	// for _, s := range seats[len(seats)-1].adgacent() {
	// 	fmt.Printf("%+v\n", s)
	// }

	for {
		var previousSeats = seats
		var newSeats = make([]seat, len(seats))
		copy(newSeats, seats)
		chaosUnderControl := true
		for i, s := range previousSeats {
			// fmt.Printf("%+v\n", s)
			allAdjEmpty := true
			occupiedAdj := 0
			for _, adj := range s.adgacent(previousSeats) {
				allAdjEmpty = allAdjEmpty && !adj.occupied
				if adj.occupied {
					occupiedAdj++
				}
			}
			if allAdjEmpty {
				newSeats[i].occupied = true
			}
			if 4 <= occupiedAdj {
				newSeats[i].occupied = false
			}
			if chaosUnderControl && previousSeats[i].occupied != newSeats[i].occupied {
				chaosUnderControl = false
			}
		}
		// for _, s := range newSeats {
		// 	fmt.Printf("%+v\n", s)
		// }

		if chaosUnderControl {
			break
		}
		seats = newSeats
	}
	ret := 0
	for _, s := range seats {
		if s.occupied {
			ret++
		}
	}
	fmt.Println(ret)
}
