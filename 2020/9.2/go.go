package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

const preamble = 25

func main() {
	var list []int
	f, err := os.Open("input")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	r := bufio.NewReader(f)
	i := 0
	invalidInt := 0
	for {
		line, err := r.ReadString('\n')
		if err != nil {
			break
		}

		currentInt, _ := strconv.Atoi(strings.TrimSuffix(line, "\n"))
		list = append(list, currentInt)

		if preamble <= i && invalidInt == 0 { // don't continue these sums if we already found invalidInt
			prev25 := list[i-preamble : i]
			// fmt.Println(prev25)
			found := false
		Loop:
			for xpos, x := range prev25 {
				for ypos, y := range prev25[:len(prev25)-1] { // don't check last one in list as it will be already sumed by the previous loops
					if xpos == ypos { // don't sum the same number with itself
						continue
					}
					if x+y == currentInt {
						// fmt.Printf("%v + %v = %v\n", x, y, currentInt)
						found = true
						break Loop
					}
				}
			}
			if !found {
				invalidInt = currentInt
			}
		}

		i++
	}
	if invalidInt == 0 {
		panic("wtf, invalidInt not found")
	}
	// fmt.Println(list)
	var consecutiveMax = len(list)
	for consecutive := 2; consecutive<consecutiveMax; consecutive++ {
		for i := range list[:len(list)-consecutive] {
			sum := 0
			for bla := 0; bla < consecutive; bla++  {
				sum += list[i+bla]
			}
			if sum == invalidInt {
				consecutiveSlice := list[i:i+consecutive]
				// fmt.Println("consecutiveSlice", i, consecutive, consecutiveSlice)
				smallest := consecutiveSlice[0]
				largest := consecutiveSlice[0]
				for _, v := range list[i:i+consecutive] {
					if v < smallest {
						smallest = v
						continue
					}
					if largest < v {
						largest = v
						continue
					}
				}
				// fmt.Println(smallest, largest, smallest+largest)
				fmt.Println(smallest+largest)
				os.Exit(0)
			}
		}
	}
}
