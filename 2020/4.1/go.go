package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type passport struct {
	byr int
	iyr int
	eyr int
	hgt string
	hcl string
	ecl string
	pid string
	cid int
}

func (p *passport) Valid() bool {
	return p.byr != 0 && p.iyr != 0 && p.eyr != 0 && p.hgt != "" && p.hcl != "" && p.ecl != "" && p.pid != "" //&& p.cid != 0
}

func readLines(filename string) ([]string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return []string{""}, err
	}
	defer f.Close()

	var ret []string

	r := bufio.NewReader(f)
	for {
		line, err := r.ReadString('\n')
		ret = append(ret, strings.Trim(line, "\n"))
		if err != nil {
			break
		}
	}
	return ret, nil
}

func parsePassports(input []string) ([]passport, error) {
	var ret []passport
	var p passport
	var empty passport
	for _, line := range input {
		if line == "" {
			if p != empty {
				ret = append(ret, p)
				p = empty
			}
			continue
		}
		fields := strings.Fields(line)
		for _, field := range fields {
			splitted := strings.SplitN(field, ":", 2)
			if len(splitted) != 2 {
				continue
			}
			switch splitted[0] {
			case "byr":
				i, err := strconv.Atoi(string(splitted[1]))
				if err == nil {
					p.byr = i
				}
			case "iyr":
				i, err := strconv.Atoi(string(splitted[1]))
				if err == nil {
					p.iyr = i
				}
			case "eyr":
				i, err := strconv.Atoi(string(splitted[1]))
				if err == nil {
					p.eyr = i
				}
			case "hgt":
				p.hgt = splitted[1]
			case "hcl":
				p.hcl = splitted[1]
			case "ecl":
				p.ecl = splitted[1]
			case "pid":
				p.pid = splitted[1]
			case "cid":
				i, err := strconv.Atoi(string(splitted[1]))
				if err == nil {
					p.cid = i
				}
			}
		}
	}
	if p != empty { // f*cken ugly, but to handle the last line…
		ret = append(ret, p)
	}
	return ret, nil
}

func main() {
	lines, err := readLines("input")
	if err != nil {
		panic(err)
	}
	passports, err := parsePassports(lines)
	if err != nil {
		panic(err)
	}
	ret := 0
	for _, p := range passports {
		if p.Valid() {
			ret++
		}
	}
	fmt.Println(ret)
}
