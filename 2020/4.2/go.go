package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type passport struct {
	byr int
	iyr int
	eyr int
	hgt string
	hcl string
	ecl string
	pid string
	cid string
}

func (p *passport) Valid() bool {
	if !strings.HasSuffix(p.hgt, "cm") && !strings.HasSuffix(p.hgt, "in") {
		return false
	}
	hgt, err := strconv.Atoi(strings.TrimSuffix(strings.TrimSuffix(p.hgt, "cm"), "in"))
	if err != nil {
		return false
	}
	switch p.hgt[len(p.hgt)-2:] {
	case "cm":
		if !(150 <= hgt && hgt <= 193) {
			return false
		}
	case "in":
		if !(59 <= hgt && hgt <= 76) {
			return false
		}
	default:
		panic("wtf")
	}
	if !strings.HasPrefix(p.hcl, "#") || len(p.hcl) != 7 {
		return false
	}
	for _, c := range p.hcl[1:] {
		if !(('0' <= c && c <= '9') || ('a' <= c && c <= 'f')) {
			return false
		}
	}
	if p.ecl != "amb" && p.ecl != "blu" && p.ecl != "brn" && p.ecl != "gry" && p.ecl != "grn" && p.ecl != "hzl" && p.ecl != "oth" {
		return false
	}

	if len(p.pid) != 9 {
		return false
	}
	for _, c := range p.pid {
		if !('0' <= c && c <= '9') {
			return false
		}
	}
	return 1920 <= p.byr && p.byr <= 2002 &&
		2010 <= p.iyr && p.iyr <= 2020 &&
		2020 <= p.eyr && p.eyr <= 2030
	//&& p.cid != ""
}

func readLines(filename string) ([]string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return []string{""}, err
	}
	defer f.Close()

	var ret []string

	r := bufio.NewReader(f)
	for {
		line, err := r.ReadString('\n')
		ret = append(ret, strings.Trim(line, "\n"))
		if err != nil {
			break
		}
	}
	return ret, nil
}

func parsePassports(input []string) ([]passport, error) {
	var ret []passport
	var p passport
	var empty passport
	for _, line := range input {
		if line == "" {
			if p != empty {
				ret = append(ret, p)
				p = empty
			}
			continue
		}
		fields := strings.Fields(line)
		for _, field := range fields {
			splitted := strings.SplitN(field, ":", 2)
			if len(splitted) != 2 {
				continue
			}
			switch splitted[0] {
			case "byr":
				i, err := strconv.Atoi(string(splitted[1]))
				if err == nil {
					p.byr = i
				}
			case "iyr":
				i, err := strconv.Atoi(string(splitted[1]))
				if err == nil {
					p.iyr = i
				}
			case "eyr":
				i, err := strconv.Atoi(string(splitted[1]))
				if err == nil {
					p.eyr = i
				}
			case "hgt":
				p.hgt = splitted[1]
			case "hcl":
				p.hcl = splitted[1]
			case "ecl":
				p.ecl = splitted[1]
			case "pid":
				p.pid = splitted[1]
			case "cid":
				p.cid = splitted[1]
			}
		}
	}
	if p != empty { // f*cken ugly, but to handle the last line…
		ret = append(ret, p)
	}
	return ret, nil
}

func main() {
	lines, err := readLines("input")
	if err != nil {
		panic(err)
	}
	passports, err := parsePassports(lines)
	if err != nil {
		panic(err)
	}
	ret := 0
	for _, p := range passports {
		if p.Valid() {
			ret++
		}
	}
	fmt.Println(ret)
}
