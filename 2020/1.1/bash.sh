#!/bin/bash

IFS=$'\n' read -d "" -ra LIST < "input"

for A in "${LIST[@]}"; do
	for B in "${LIST[@]}"; do
		SUM=$(( A + B ))
		if [[ $SUM == 2020 ]]; then
			echo "$A $B"
			echo "$(( A * B ))"
			exit
		fi
	done
done
