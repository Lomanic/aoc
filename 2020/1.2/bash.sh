#!/bin/bash

IFS=$'\n' read -d "" -ra LIST < "input"

for A in "${LIST[@]}"; do
	for B in "${LIST[@]}"; do
		for C in "${LIST[@]}"; do
			SUM=$(( A + B + C ))
			if [[ $SUM == 2020 ]]; then
				echo "$A $B $C"
				echo "$(( A * B * C ))"
				exit
			fi
		done
	done
done
