#!/usr/bin/env python

import sys

f = open('input','r')
the_list = []
while True:
    x = f.readline()
    x = x.rstrip()
    if not x: break
    the_list.append(int(x))

for a in the_list:
    for b in the_list:
        for c in the_list:
            if (a+b+c) == 2020:
                print("{} {} {}".format(a, b, c))
                print(a*b*c)
                sys.exit(0)
