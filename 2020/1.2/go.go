package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strconv"
)

func main() {

	// Read entire file content, giving us little control but
	// making it very simple. No need to close the file.
	content, err := ioutil.ReadFile("input")
	if err != nil {
		log.Fatal(err)
	}

	var list []int
	for _, v := range bytes.Split(content, []byte("\n")) {
		i, err := strconv.Atoi(string(v))
		if err != nil {
			continue
		}
		list = append(list, i)
	}
	for _, a := range list {
		for _, b := range list {
			for _, c := range list {
				if (a + b + c) == 2020 {
					fmt.Println(a, b, c)
					fmt.Println(a * b * c)
					os.Exit(0)
				}
			}
		}
	}
}
