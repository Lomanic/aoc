#!/usr/bin/env python

def parse_input(file):
    f = open(file, 'r')
    the_list = []
    while True:
        line = f.readline()
        line = line.rstrip()
        if not line: break
        the_list.append(line)
    return the_list

def traverse(the_list, x_increment, y_increment):
    path = ""
    x = 0
    y = 0
    while True:
        x += x_increment
        y += y_increment
        if x > len(the_list) - 1:
            break
        path += the_list[x][y % len(the_list[x])]
    return len([c for c in path if c == "#"])

the_list = parse_input("input")

sum = 1
for i in [[1, 1], [1, 3], [1, 5], [1, 7], [2, 1]]:
    s = traverse(the_list, i[0], i[1])
    sum = sum * s

print(sum)
