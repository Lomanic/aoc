#!/bin/bash

IFS=$'\n' read -d "" -ra LIST < "input"

VALID=0
for LINE in "${LIST[@]}"; do
	POLICY=${LINE%%:*}
    LETTER=${POLICY##* }
    OCCURENCES=${POLICY%% *}
    POS1=${OCCURENCES%%-*}
    POS2=${OCCURENCES##*-}
    PASSWORD=${LINE##*:}
    PASSWORD=${LINE##* }

    FOUND=0
    POS1=$(( POS1 -1 )) # 0 indexed
    POS2=$(( POS2 -1 )) # 0 indexed
    if [[ "${PASSWORD:$POS1:1}" == "$LETTER" ]]; then
        FOUND=$(( FOUND + 1 ))
    fi
    if [[ "${PASSWORD:$POS2:1}" == "$LETTER" ]]; then
        FOUND=$(( FOUND + 1 ))
    fi
    if [[ $FOUND == 1 ]]; then
        VALID=$(( VALID + 1 ))
    fi
done

echo $VALID
